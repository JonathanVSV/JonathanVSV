### Hi 👋

I am Jonathan V. Solórzano, a biologist / geographer interested in using remote sensing and spatial analyses to study tropical forests 🌳🛰️. I am particularly interested in monitoring deforestation and forest degradation, as well as modelling the aboveground biomass of these forests 🤓.

I usually use R and GEE to make my analyses, and QGIS to visualize or perform other simple tasks.

You can visit this page to know more about my research or take look at my posts (mainly R code): https://jonathanvsv.github.io/Ppage2/
